const express = require('express');
const v1 = require('./v1');
const errorHandler = require('api-error-handler');

const router = express.Router();

/**
 *  First version of api
 */
router.use('/v1', v1);

/**
 * Error handler
 */
router.all('*', (req, res, next) => {
  const { method,url } = req;
  const err = new Error(`Route ${method} ${url} not Found`);
  err.status = 404;
  next(err);
}, errorHandler());

module.exports = router;
