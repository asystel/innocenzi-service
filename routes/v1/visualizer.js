const express = require('express');

const router = express.Router();

const cors = require('cors')

const TagVisualizer = require('../../controllers/tagVisualizer');

/* POST users workInProgress. */
router.get('/', cors(), TagVisualizer.resurces);

router.get('/:codiceRisorsaTecnologica', cors(), TagVisualizer.tagsResurce);

module.exports = router;