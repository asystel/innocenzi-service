const express = require('express');
const tags = require('./visualizer');
const updtags = require('./updater');

const router = express.Router();

router.use('/visualizeTag', tags);
router.use('/updateTag', updtags);

module.exports = router;
