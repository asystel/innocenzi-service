const express = require('express');

const router = express.Router();

const cors = require('cors')

const TagVisualizer = require('../../controllers/tagVisualizer');

router.post('/:codiceRisorsaTecnologica', cors(), TagVisualizer.setTimeFromResurseButton);

module.exports = router;