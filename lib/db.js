const pg = require('pg');

const env = process.env.NODE_ENV || 'development';
const config = require('../database/config/pgConfig.json')[env];

// create a config json file '../database/config/pgConfig.json'
// note: all config is optional and the environment variables
// will be read if the config is not present
const configDefault = {
  user: 'asystel',
  host: '192.168.1.250',
  database: 'innocenzi',
  password: 'asystel',
  port: 5432,
  max: 10, // max number of clients in the pool
  idleTimeoutMillis: 30000 // how long a client is allowed to remain idle before being closed
};

const pool = new pg.Pool(config || configDefault);

pool.on('error', (err, client) => {
  console.error('idle client error', err.message, err.stack);
});

// export the query method for passing queries to the pool
module.exports.query = (text, values, callback) => {
  console.log('query:', text, values);
  return pool.query(text, values, callback);
};

// the pool also supports checking out a client for
// multiple operations, such as a transaction
module.exports.connect = callback => pool.connect(callback);
module.exports.pool = pool;
