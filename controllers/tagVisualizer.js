const DB = require('../lib/db');

exports.resurces = (request, result, next) => {
  const p = DB.pool;
  p.connect((err, client, done) => {
    if (err) next(err);
		if (client){
			client.query('SELECT codice_risorsa_tecnologica, descrizione_risorsa_tecnologica ' +
									 'FROM public.risorse_tecnologiche ' +
									 'WHERE codice_risorsa_tecnologica IN (SELECT risorsa_tecnologica ' + 
									 '                                     FROM dati_processo.configurazione_acquisizione)', 

										(queryError, res) => {
											done();
											if (queryError) {
												next(queryError);
											}
											else {
												console.log(res.rows);
												result.send(res.rows);
											}
										});
		};
  });
};

exports.tagsResurce = (request, result, next) => {
  const p = DB.pool;
  const { codiceRisorsaTecnologica } = request.params;
  p.connect((err, client, done) => {
    if (err) next(err);
		if (client){
			client.query('SELECT * ' +
									 'FROM dati_processo.configurazione_acquisizione ca ' +
									 '  LEFT JOIN acq_custom.tempo_tasto_risorsa ttr ON(ttr.codice_risorsa_tecnologica = ca.risorsa_tecnologica) ' +      
									 'WHERE risorsa_tecnologica = $1 ',
									 [codiceRisorsaTecnologica], 
									 (queryError, res) => {
										done();
										if (queryError) {
											next(queryError);
										}
										else {
											console.log(res.rows);
											result.send(res.rows);
										}
									});
		};
  });
};

exports.setTimeFromResurseButton = (request, result, next) => {
  const p = DB.pool;
  const { body } = request;
	//const { codiceRisorsaTecnologica } = body;
	const { codiceRisorsaTecnologica } = request.params;
	console.log('update ', codiceRisorsaTecnologica);	
  p.connect((err, client, done) => {
    if (err) next(err);
    client.query('UPDATE acq_custom.tempo_tasto_risorsa SET data_ora_tasto=current_timestamp WHERE codice_risorsa_tecnologica=$1;', [codiceRisorsaTecnologica], (queryError, res) => {
			done();
      if (queryError) {
        next(queryError);
      }else{
				result.send(res.rows);
			};
    });
    /*client.query('INSERT INTO acq_custom.tempo_tasto_risorsa(codice_risorsa_tecnologica, data_ora_tasto) VALUES ($1, current_timestamp);', [codiceRisorsaTecnologica], (queryError, res) => {
			console.log('insert ', codiceRisorsaTecnologica);
			done();
      if (queryError) {
        next(queryError);
      }
      else {
        result.send(res.rows);
      }
    });*/
  });
};